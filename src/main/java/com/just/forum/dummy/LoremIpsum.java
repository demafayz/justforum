package com.just.forum.dummy;

public class LoremIpsum {

    public static String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    public static String LOREM_SHORT = LOREM_IPSUM.substring(0, 20);
    public static String LOREM_MID = LOREM_IPSUM.substring(0, 40);
    public static String LOREM_LONG = LOREM_IPSUM.substring(0, 100);
}
