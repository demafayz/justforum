package com.just.forum.data.mappers;

import com.just.forum.db.entity.UserDbEntity;
import com.just.forum.domain.entity.PersonEntity;
import com.just.forum.domain.entity.user.UserEntity;

public class UserMapper {

    public static UserDbEntity toDbEntity(UserEntity userEntity) {
        UserDbEntity result = new UserDbEntity();
        result.setId(userEntity.getId());
        result.setUserName(userEntity.getUserName());
        result.setPassword(userEntity.getPassword());
        result.setFirstName(userEntity.getFirstName());
        result.setLastName(userEntity.getLastName());
        return result;
    }

    public static UserEntity toEntity(UserDbEntity userDbEntity) {
        UserEntity result = new UserEntity();
        result.setId(userDbEntity.getId());
        result.setUserName(userDbEntity.getUserName());
        result.setPassword(userDbEntity.getPassword());
        result.setFirstName(userDbEntity.getFirstName());
        result.setLastName(userDbEntity.getLastName());
        return result;
    }

    public static PersonEntity toPersonEntity(UserEntity creator) {
        PersonEntity personEntity = new PersonEntity();
        personEntity.setId(creator.getId());
        personEntity.setName(creator.getFirstName());
        personEntity.setSurname(creator.getLastName());
        return personEntity;
    }
}
