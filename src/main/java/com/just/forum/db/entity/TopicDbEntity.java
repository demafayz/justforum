package com.just.forum.db.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table
public class TopicDbEntity {

    @Id
    private String id;
    private String title;
    private String description;

    private String creator_person_id;

    private String comment_ids;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatorPersonId() {
        return creator_person_id;
    }

    public void setCreatorPersonId(String creatorPersonId) {
        this.creator_person_id = creatorPersonId;
    }

    public List<String> getCommentIds() {
        if (comment_ids == null || comment_ids.isBlank()) {
            return new ArrayList<>();
        } else {
            return new ArrayList<>(Arrays.asList(comment_ids.split(",")));
        }
    }

    public void setCommentIds(List<String> commentIds) {
        this.comment_ids = String.join(",", commentIds);
    }
}