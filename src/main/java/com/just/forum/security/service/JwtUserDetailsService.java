package com.just.forum.security.service;

import java.util.ArrayList;

import com.just.forum.data.mappers.UserMapper;
import com.just.forum.db.entity.UserDbEntity;
import com.just.forum.domain.entity.user.UserEntity;
import com.just.forum.domain.repository.UserRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Deprecated
//@Service
public class JwtUserDetailsService /*implements UserDetailsService*/ {

	private final UserRepository userRepository;

	public JwtUserDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void createUser(UserEntity userEntity) {
		UserDbEntity userDbEntity = UserMapper.toDbEntity(userEntity);
		try {
			loadUserByUsername(userDbEntity.getUserName());
		} catch (UsernameNotFoundException ex) {
			userRepository.save(userDbEntity);
		}

		userRepository.save(userDbEntity);
	}

//	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if ("javainuse".equals(username)) {
			return new User("javainuse", "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6",
					new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}

}