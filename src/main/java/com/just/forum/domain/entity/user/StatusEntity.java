package com.just.forum.domain.entity.user;

public enum StatusEntity {
    ACTIVE, NOT_ACTIVE, DELETED
}