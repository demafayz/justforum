package com.just.forum.domain.entity.user;

import lombok.Data;

@Data
public class UserEntity {

    private String id;

    private String userName;

    private String firstName;

    private String lastName;

    private String password;
}