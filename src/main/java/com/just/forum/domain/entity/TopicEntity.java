package com.just.forum.domain.entity;

import java.util.ArrayList;
import java.util.List;

public class TopicEntity {
    private String id;
    private String title;
    private String description;
    private PersonEntity creator;
    private List<CommentEntity> comments = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PersonEntity getCreator() {
        return creator;
    }

    public void setCreator(PersonEntity creator) {
        this.creator = creator;
    }

    public List<CommentEntity> getComments() {
        return comments;
    }

    public void setComments(List<CommentEntity> comments) {
        this.comments = comments;
    }
}
