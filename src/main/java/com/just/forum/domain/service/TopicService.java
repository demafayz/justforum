package com.just.forum.domain.service;

import com.just.forum.domain.entity.TopicEntity;

import java.util.List;

public interface TopicService {

    List<TopicEntity> getAllEntity();

    TopicEntity findTopicById(String topicId);

    void createTopic(TopicEntity topic);

    void editTopic(TopicEntity topic);

    void deleteTopic(String topicId);
}
