package com.just.forum.domain.service.impl;

import com.just.forum.data.mappers.TopicMapper;
import com.just.forum.data.mappers.UserMapper;
import com.just.forum.db.entity.CommentDbEntity;
import com.just.forum.db.entity.TopicDbEntity;
import com.just.forum.db.entity.UserDbEntity;
import com.just.forum.domain.entity.CommentEntity;
import com.just.forum.domain.entity.PersonEntity;
import com.just.forum.domain.entity.user.UserEntity;
import com.just.forum.domain.repository.CommentRepo;
import com.just.forum.domain.repository.TopicRepo;
import com.just.forum.domain.repository.UserRepository;
import com.just.forum.domain.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class CommentServiceImpl implements CommentService {

    private TopicRepo topicRepo;
    private UserRepository userRepository;
    private CommentRepo commentRepo;

    @Autowired
    public CommentServiceImpl(TopicRepo topicRepo, UserRepository userRepository, CommentRepo commentRepo) {
        this.topicRepo = topicRepo;
        this.userRepository = userRepository;
        this.commentRepo = commentRepo;
    }

    @Override
    public List<CommentEntity> getAllComments(String topicId) {
        List<CommentDbEntity> allDbComments = commentRepo.findAll();
        List<CommentEntity> result = new ArrayList<>();
        for (int i = 0; i < allDbComments.size(); i++) {
            CommentDbEntity currentDbComment = allDbComments.get(i);
            if (currentDbComment.getTopicId().equals(topicId)) {
                UserDbEntity creator = userRepository.findById(currentDbComment.getCreatorId()).orElse(null);
                CommentEntity commentEntity = TopicMapper.dbToEntityComment(currentDbComment, creator);
                result.add(commentEntity);
            }
        }
        return result;
    }

    @Override
    public CommentEntity createComment(String topicId, String comment, UserEntity creator) {
        TopicDbEntity dbTopic = topicRepo.findById(topicId).orElseThrow();


        String commentId = UUID.randomUUID().toString();
        Date currentDate = new Date();
        PersonEntity creatorPersonEntity = UserMapper.toPersonEntity(creator);

        CommentEntity newComment = new CommentEntity(commentId, comment, currentDate, creatorPersonEntity);
        CommentDbEntity newDbCommentEntity = TopicMapper.toDbComment(newComment, topicId);
        commentRepo.save(newDbCommentEntity);

        List<String> topicCommentIds = dbTopic.getCommentIds();
        topicCommentIds.add(commentId);
        dbTopic.setCommentIds(topicCommentIds);
        topicRepo.save(dbTopic);
        return newComment;
    }

    @Override
    public void deleteComment(String topicId, String commentId) {
        TopicDbEntity topicDbEntity = topicRepo.findById(topicId).orElseThrow();
        List<String> commentIds = topicDbEntity.getCommentIds();
        commentIds.removeIf(dbCommentIDs -> dbCommentIDs.equals(commentId));
        topicDbEntity.setCommentIds(commentIds);
        topicRepo.save(topicDbEntity);
        commentRepo.deleteById(commentId);
    }
}