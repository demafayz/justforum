package com.just.forum.domain.service;

import com.just.forum.domain.entity.CommentEntity;
import com.just.forum.domain.entity.user.UserEntity;

import java.util.List;

public interface CommentService {

    List<CommentEntity> getAllComments(String topicId);

    CommentEntity createComment(String topicId, String comment, UserEntity creator);

    void deleteComment(String topicId, String commentId);
}