package com.just.forum.controller;

import com.just.forum.domain.entity.CommentEntity;
import com.just.forum.domain.entity.TopicEntity;
import com.just.forum.domain.entity.user.UserEntity;
import com.just.forum.domain.service.CommentService;
import com.just.forum.domain.service.UserService;
import com.just.forum.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("comment")
public class CommentController {

    private CommentService commentService;

    private UserService userService;

    @Autowired
    public CommentController(CommentService commentService, UserService userService) {
        this.commentService = commentService;
        this.userService = userService;
    }

    @GetMapping
    public List<CommentEntity> getAllComments(@PathParam("topicId") String topicId) {
        List<CommentEntity> allCommentList = commentService.getAllComments(topicId);
        return allCommentList;
    }

    @PutMapping("create")
    public void addComment(@PathParam("topicId") String topicId, @RequestBody CommentEntity comment) {
        UserEntity creator = getCurrentUser();
        commentService.createComment(topicId, comment.getComment(), creator);
    }

    @DeleteMapping("delete")
    public void deleteComment(@PathParam("topicId") String topicId, @PathParam("commentId") String commentId) {
        commentService.deleteComment(topicId, commentId);
    }

    private TopicEntity getTopicById(String topicId) {
        List<TopicEntity> topicList = getAllTopic();
        TopicEntity currentEntity = null;
        for (int i = 0; i < topicList.size(); i++) {
            if (topicList.get(i).getId().equals(topicId)) {
                currentEntity = topicList.get(i);
                break;
            }
        }
        if (currentEntity != null) {
            return currentEntity;
        }
        throw new NotFoundException();
    }

    private List<TopicEntity> getAllTopic() {
        return TopicController.topicProvider.getAll();
    }

    private UserEntity getCurrentUser() {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return userService.findByUsername(userName);
        } catch (Throwable e) {
            return null;
        }
    }
}
